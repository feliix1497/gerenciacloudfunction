const admin = require("firebase-admin");
admin.initializeApp()


exports.candidate_addCandidate = require("./app/candidate/addCandidate")
exports.candidate_getCandidate = require("./app/candidate/getCandidate")
exports.candidate_saveFormResult = require("./app/candidate/saveFormResult")
exports.candidate_deleteCandidates = require("./app/candidate/deleteCandidate")