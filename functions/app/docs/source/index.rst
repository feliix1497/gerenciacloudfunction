.. Gerencia documentation master file, created by
   sphinx-quickstart on Sat Aug  3 17:13:48 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GerenciaCloudFunctions's documentation!
===================================================
Documentaciones para los Cloud Functions utilizados para el proyecto de Reclutamiento Interno.
Se utilizo las funciones hechas en python en la cual puede encontrar el repositorio en https://gitlab.com/feliix1497/gerenciacloudfunction.git

Las librerias que fueron utilizados fueron: 
	- logging
	- flask
	- firebase_admin
	- xmltodict
	- gcloud


.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

.. automodule:: main
   :members:


* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
