import statistics
from firebase_admin import firestore
from firebase_admin import credentials
db = firestore.client()

class Candidate: 
    def __init__(self, name = None, email = None, phone = None , identification = None):
        self.name = name
        self.email = email
        self.phone = phone
        self.identification = identification

    def add_candidate(self):
        data = { 
            u'name': self.name,
            u'email': self.email,
            u'phone': self.phone,
            u'identification': self.identification,
            u'quizResult': {}
        }
        db.collection(u'candidates').add(data)
        return data
    
    def delete(self, batch_size): 
        docs = db.collection(u'candidates').limit(batch_size).get()
        deleted = 0

        for doc in docs: 
            print(u'Deleting doc {} => {}'.format(doc.id, doc.to_dict()))
            doc.reference.delete()
            deleted = deleted + 1
        
        if deleted >= batch_size: 
            return self.delete(batch_size)


    def save_form_result(self, user_id, quizResult, comment): 
        data = { 
            u'quizResult': quizResult,
            u'comment': comment
        }

        db.collection(u'candidates').document(user_id).set(data, merge=True)
        return { 'user_id': user_id, 'quizResult': quizResult, 'comment': comment }

    def get_sorted_candidates(self): 
        candidates = db.collection(u'candidates').get()
        to_sort_candidate = []
        for candidate in candidates:
            data = candidate.to_dict()
            user_id = candidate.id
            user_name = data["name"]
            quizResult = data["quizResult"]
            if (quizResult):
                quiz_info = [quizResult["comunication"], quizResult["drive"], quizResult["skill"]]
                average = statistics.mean(quiz_info)
                to_sort_data = { 
                    "user_id": user_id, 
                    "user_name": user_name,
                    "quizResult": quizResult, 
                    "average": average
                }
                to_sort_candidate.append(to_sort_data)

        
        newlist = sorted(to_sort_candidate, key=lambda k: k['average'], reverse=True) 
        return newlist