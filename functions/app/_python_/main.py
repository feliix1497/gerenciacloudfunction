import flask
import firebase_admin
import json
import logging
import sys
import xmltodict
from firebase_admin import credentials

#credentials are declared due to sphinx.
cred = credentials.Certificate("/home/felix/Documents/JavascriptProjects/Gerencia/serviceAccount.json")
firebase_admin.initialize_app(cred)

from firebase_admin import firestore
from firebase_admin import credentials
from _helper_ import candidate as helper

#Add Cors function.
def add_cors(response, code = 200): 
    #Add Cors function.
    headers = { 'Access-Control-Allow-Origin': '*'} 
    return (response, code, headers)


def add_candidate(request): 
    """
    Servicio que agrega un candidato hacia la coleccion de /candidates/
    Acepta un request POST en la cual contiene los siguientes campos en el body: 

    :type name: string
    :param name: Nombre del usuario.
    
    :type email: string
    :param email: el email del usuario.

    :type phone: string
    :param phone: El numero para contactar al usuario.

    :type identification: string
    :param identification: La cedula del usuario.
    
    :returns: {'success': True, 'data': {name: string, email: string, phone: string, identification: string, quizResult: {}}, 'message': 'Cadidate Sucessfully Added.'}
    """

    # try: 
    # Set CORS headers for the preflight request
    if request.method == 'OPTIONS':
        # Allows GET requests from any origin with the Content-Type
        # header and caches preflight response for an 3600s
        headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST,GET',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600'
        }

        return ('', 204, headers)
    
    request_json = request.get_json(silent=True)
    name = request_json['name']
    email = request_json['email']
    phone = request_json['phone']
    identification = request_json['identification']

    Candidate = helper.Candidate(name, email, phone, identification)
    candidate_data = Candidate.add_candidate()
    print('Candidate successfully added.', candidate_data)

    result = json.dumps({'success': True, 'data': candidate_data, 'message': 'Cadidate Sucessfully Added.'})
    return add_cors(result)


def delete_candidate(request): 
    """
    Servicio que borra a TODOS los candidatos del collection /candidates/
    en batch de 30.
    Acepta un request DELETE. No requiere un body. 

    :returns: {'success': True, 'message': 'Users succesfully deleted'}
    """
    try: 


        helper.Candidate().delete(30)
        result = json.dumps({'success': True, 'message': 'Users succesfully deleted'})
        return add_cors(result)
    except: 
        print("Something wrong getting candidate")
        logging.error(sys.exc_info())
        result = json.dumps({'success': False, 'message': "Something wrong getting candidates."})
        return add_cors(result, 500)

def save_form_result(request): 

    """
    Servicio que guarda el resultado de los pesos que contiene cada categoria de cada usuario.
    Request Method: POST

    :type user_id: string
    :param user_id: El uid del usuario dentro del Firestore.

    :type quizResult: dict
    :param quizResult: El diccionario que contiene:  { comunication: type->integer, drive: type->integer, skill: type->integer }

    :returns: {'success': True, 'data': {comunication: integer, skill: integer, drive: integer}, 'message': 'Success saved.'}

    """
    
    try: 
        request_json = request.get_json(silent=True)
        user_id = request_json['user_id']
        quizResult = request_json['quizResult']
        comment = request_json['comment']

        form_result = helper.Candidate().save_form_result(user_id,quizResult,comment)
        result = json.dumps({'success': True, 'data': form_result, 'message': 'Success saved.'})
        return add_cors(result)

    except: 
        result = json.dumps({'success': False, 'message': 'Something wrong saving form.'})
        return add_cors(result, 500)


def get_sorted_users(request):
    """
    Servicio que recoge todos los usuarios del collection /candidates/
    y lo ordena por el promedio de los resultados obtenidos en quizResult

    Metodo: GET

    :returns: {'success': True, 'data': [{'user_id': string, 'user_name': string, 'quizResult': dict, 'average': integer}] , 'message': 'Success retrieving sorted candidates..'}


    """
    try:
        sorted_candidates = helper.Candidate().get_sorted_candidates()
        result = json.dumps({'success': True, 'data': sorted_candidates, 'message': 'Success retrieving sorted candidates..'})
        return add_cors(result)
    except:
        result = json.dumps({'success': False, 'message': 'Something wrong retrieving data'})
        return add_cors(result, 500)
